import { Request, Response } from 'express';

const getAppStatus = (_: Request, res: Response): Response => {
  return res.send('OK');
};
const AppController = {
  getAppStatus,
};
export default AppController;
