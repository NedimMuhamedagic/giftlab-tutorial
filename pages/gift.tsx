import { withRouter } from 'next/router';
import React from 'react';
import Layout from '../hoc/Layout';

const Page = withRouter(props => {
  return (
    <Layout>
      <h1>{props.router.query.id}</h1>
      <p>This is the page content.</p>
    </Layout>
  );
});

export default Page;
