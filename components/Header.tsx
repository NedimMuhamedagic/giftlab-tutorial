import React from 'react';

import { Layout } from '../styled';
import {
  HeaderContainer,
  InternalMenuContainer,
  InternalMenuElement,
  LogoImg,
  MenuContainer,
  SearchContainer,
  SearchIconContainer,
  SearchInput,
} from '../styled/header';

const Header = (): JSX.Element => (
  <HeaderContainer>
    <MenuContainer>
      <LogoImg src='http://placehold.it/120x25' />
      <InternalMenuContainer>
        <InternalMenuElement>home</InternalMenuElement>
        <InternalMenuElement>explore</InternalMenuElement>
      </InternalMenuContainer>
      <Layout flex={1}>
        <Layout width={450} height={50} display='flex'>
          <SearchContainer>
            <SearchIconContainer>
              <img src='http://placehold.it/20x20' />
            </SearchIconContainer>
            <SearchInput />
          </SearchContainer>
        </Layout>
      </Layout>
      <div>Sign in or create an account</div>
    </MenuContainer>
  </HeaderContainer>
);

export default Header;
